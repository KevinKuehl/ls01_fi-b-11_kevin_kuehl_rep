import java.util.Scanner;

public class Übungsblatt_AB_Konsoleneingabe_Aufgabe_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner myScanner =new Scanner(System.in);
		
		//Aufgabe: Begrüßen Sie den Nutzer und fragen ihn nach seinem Namen und seinem Alter.
		//Speichern Sie beide Eingaben in Variablen und geben diese am Ende des 
		//Programms in der Konsole aus
		
		String name = " "; 
		int alter;
		
		System.out.println("Willkommen neue Lebenform. Wie lautet Ihr Name?");
		name = myScanner.next();
		System.out.println(name);
		
		System.out.println("Ein schrecklicher Name");
		
		System.out.println("Wie alt sind Sie denn?");
		alter = myScanner.nextInt(); 
		System.out.println(alter);
			
		myScanner.close() ;
				
	}

}
