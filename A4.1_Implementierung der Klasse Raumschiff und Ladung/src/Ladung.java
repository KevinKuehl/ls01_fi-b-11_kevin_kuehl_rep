
public class Ladung {

	// Attribute
	private String Bezeichnung;
	private int Menge;

	// Konstruktor
	public Ladung(String Bezeichnung, int Menge) {
		this.Bezeichnung = Bezeichnung;
		this.Menge = Menge;
	}

	// Verwaltungsmethoden
	public String getBezeichnung() {
		return Bezeichnung;
	}

	public void setBezeichnung(String Bezeichnung) {
		this.Bezeichnung = Bezeichnung;
	}

	public int getMenge() {
		return Menge;
	}

	public void setMenge(int Menge) {
		this.Menge = Menge;
	}

	// weitere Methoden
	public String getKennung() {
		return this.Bezeichnung + " " + this.Menge;
	}

	@Override
	public String toString() {
		return this.Bezeichnung + " " + this.Menge;
	}
}