import java.util.ArrayList;

public class Raumschiff {

	// Attribute des Raumschiffes
	private String schiffsname;
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int zustandSchildeInProzent;
	private int zustandHuelleInProzent;
	private int zustandLebenserhaltungssystemeInProzent;
	private int anzahlDroiden;
	static private ArrayList<String> BroadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> Ladungsverzeichnis = new ArrayList<Ladung>();

	// Konstruktoren des Raumschiffes

	public Raumschiff(String schiffsname, int photonentorpedoAnzahl, int energieversorgungInProzent,
			int zustandHuelleInProzent, int zustandLebenserhaltungssystemeInProzent, int anzahlDroiden) {
		this.schiffsname = schiffsname;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.zustandHuelleInProzent = zustandHuelleInProzent;
		this.zustandLebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
		this.anzahlDroiden = anzahlDroiden;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getZustandHuelleInProzent() {
		return zustandHuelleInProzent;
	}

	public void setZustandHuelleInProzent(int zustandHuelleInProzent) {
		this.zustandHuelleInProzent = zustandHuelleInProzent;
	}

	public int getZustandLebenserhaltungssystemeInProzent() {
		return zustandLebenserhaltungssystemeInProzent;
	}

	public void setZustandLebenserhaltungssystemeInProzent(int zustandLebenserhaltungssystemeInProzent) {
		this.zustandLebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
	}

	public int getAnzahlDroiden() {
		return anzahlDroiden;
	}

	public void setAnzahlDroiden(int anzahlDroiden) {
		this.anzahlDroiden = anzahlDroiden;
	}

	// weiteres
	public void addLadung(Ladung neueLadung) {
		Ladungsverzeichnis.add(neueLadung);
	}
	
	public void zustandAusgeben() {
		System.out.println("Schiffsname" + this.schiffsname + "\n" + "photonentorpedoAnzahl" + this.photonentorpedoAnzahl + "\n" 
			+ "energieversorgungInProzent" + this.energieversorgungInProzent + "\n" +  "zustandSchildeInProzent" + this.zustandSchildeInProzent
			+ "\n" + "zustandHuelleInProzent" + this.zustandHuelleInProzent + "\n" + "zustandLebenserhaltungssystemeInProzent" + this.zustandLebenserhaltungssystemeInProzent 
			+ "\n" + "anzahlDroiden" + this.anzahlDroiden + "\n") ;
	}
	public void ladungsverzeichnisAusgeben() {
		System.out.printf("Die nachfolgende Ladung ist auf dem Raumschiff %s vorhanden\n", this.schiffsname);
		for (int i = 0; i < Ladungsverzeichnis.size(); i++) {
			System.out.printf("%2s : %3d\n", Ladungsverzeichnis.get(i).getBezeichnung(),
					Ladungsverzeichnis.get(i).getMenge());
		}
		System.out.println("\n");
		}
	
	public void torpedosAbfeuern(Raumschiff ziel) {
		if (this.photonentorpedoAnzahl > 0) {
			this.photonentorpedoAnzahl--;
			System.out.println("Photonentorpedo abgeschossen \n");
			ziel.trefferMerken(ziel);
		} else {
			nachrichtAnAlle("-=*Click*=-");
		}

	}

	public void phaserAbfeuern(Raumschiff ziel) {
		if (this.energieversorgungInProzent > 49) {
			this.energieversorgungInProzent = this.energieversorgungInProzent - 50;
			System.out.println("Phaserkanone abgeschossen \n");
			ziel.trefferMerken(ziel);
		} else {
			nachrichtAnAlle("-=*Click*=-");
		}
	}

	private void trefferMerken(Raumschiff ziel) {
		System.out.println(this.schiffsname + "wurde geroffen\n");
		this.zustandSchildeInProzent = this.zustandSchildeInProzent - 50;
		if (this.zustandSchildeInProzent < 1) {
			this.zustandHuelleInProzent = this.zustandHuelleInProzent - 50;
			this.energieversorgungInProzent = this.energieversorgungInProzent - 50;
		}
		if (this.zustandHuelleInProzent < 1) {
			this.zustandLebenserhaltungssystemeInProzent = 0;
			System.out.println("Die Lebenserhaltungssysteme vom Raumschiff " + this.schiffsname + "wurden zerstört\n");
		}

	}

	public void nachrichtAnAlle(String nachricht) {
		BroadcastKommunikator.add(nachricht);
	}

	static public String logbuchAusgeben() {
		String x = "";
		for (int i = 0; i < BroadcastKommunikator.size(); i++) {
			x = x + BroadcastKommunikator.get(i);
			x = x + "; ";
		}
		return x;
	}

	public void torpedosLaden(int anzahl) {
		boolean vorhanden = false;
		for (int i = 0; i < this.Ladungsverzeichnis.size(); i++) {
			if (this.Ladungsverzeichnis.get(i).getBezeichnung().equals("Photonentorpedo")) {
				vorhanden = true;
				if (this.Ladungsverzeichnis.get(i).getMenge() <= anzahl) {
					this.photonentorpedoAnzahl = this.photonentorpedoAnzahl + this.Ladungsverzeichnis.get(i).getMenge();
					if (this.Ladungsverzeichnis.get(i).getMenge() > 1) {
						System.out.println(
								this.Ladungsverzeichnis.get(i).getMenge() + " Photonentorpedos wurden eingesetzt\n");
					} else {
						System.out.println(
								this.Ladungsverzeichnis.get(i).getMenge() + " Photonentorpedo wurde eingesetzt\n");
					}
					this.Ladungsverzeichnis.remove(i);
				} else {
					this.photonentorpedoAnzahl = this.photonentorpedoAnzahl + anzahl;
					System.out.println(anzahl + "Photonentorpedos wurden eingesetzt \n");
					anzahl = this.Ladungsverzeichnis.get(i).getMenge() - anzahl;
					this.Ladungsverzeichnis.get(i).setMenge(anzahl);
				}
				break;
			}
		}
		if (vorhanden == false) {
			System.out.println("Keine Photonentorpedos gefunden \n");
			nachrichtAnAlle("-=*Click*=-");
		}

	}

	public void ladungsverzeichnisAufräumen() {
		for (int i = this.Ladungsverzeichnis.size() - 1; i >= 0; i--) {
			if (this.Ladungsverzeichnis.get(i).getMenge() == 0)
				this.Ladungsverzeichnis.remove(i);
		}

	}

	public void reparieren(boolean schilde, boolean energieversorgung, boolean huelle, int anzahl) {
		int AnzahlderauftruegesetztenSchiffsstrukturen = 0;
		if (schilde == true) {
			AnzahlderauftruegesetztenSchiffsstrukturen++;
		}
		if (energieversorgung == true) {
			AnzahlderauftruegesetztenSchiffsstrukturen++;
		}
		if (huelle == true) {
			AnzahlderauftruegesetztenSchiffsstrukturen++;
		}
		if (AnzahlderauftruegesetztenSchiffsstrukturen == 0) {
			System.out.println("Sie müssen mindestens eine Schiffsstruktur reparieren\n");
			return;
		}

		if (anzahl < 1) {
			System.out.println("Sie müssen mindestens einen Reperaturandroiden einsetzen\n");
			return;
		}
		int zufall = (int) (Math.random() * ((100 - 0) + 1));
		if (anzahl > this.anzahlDroiden) {
			anzahl = this.anzahlDroiden;
		}

		int reperaturinProzent = (zufall * anzahl) / AnzahlderauftruegesetztenSchiffsstrukturen;
		if (schilde == true) {
			this.zustandSchildeInProzent = this.zustandSchildeInProzent + reperaturinProzent;
		}
		if (energieversorgung == true) {
			this.energieversorgungInProzent = this.energieversorgungInProzent + reperaturinProzent;
		}
		if (huelle == true) {
			this.zustandHuelleInProzent = this.zustandHuelleInProzent + reperaturinProzent;
		}

	}
}