
public class RaumschiffTest {

	public static void main(String[] args) {
	
	//Erzeugen der Objekte
	Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 1, 100, 100, 100, 2);
	Raumschiff romulaner = new Raumschiff("IRW Khazara", 2, 100, 100, 100, 2);
	Raumschiff vulkanier = new Raumschiff("Ni'Var", 0, 80, 80, 50, 5);
		
	//Erzeugen der Ladung Objekte
	Ladung klingonenLadung1 = new Ladung("Bat´leth Klingonen Schwert", 200);
	Ladung klingonenLadung2 = new Ladung("Ferengi Schneckensaft", 200);

	Ladung romulanerLadung1 = new Ladung("Borg-Schrott", 5);
	Ladung romulanerLadung2 = new Ladung("Rote Materie", 2);
	Ladung romulanerLadung3 = new Ladung("Plasma-Waffe", 50);

	Ladung vulkanierLadung1 = new Ladung("Forschungssonde", 35);
	Ladung vulkanierLadung2 = new Ladung("Photonentorpedo", 3);
	
	//Ladung den Raumschiffen zuordnen
	klingonen.addLadung(klingonenLadung1);
	klingonen.addLadung(klingonenLadung2);
	
	romulaner.addLadung(romulanerLadung1);
	romulaner.addLadung(romulanerLadung2);
	romulaner.addLadung(romulanerLadung3);
	
	vulkanier.addLadung(vulkanierLadung1);
	vulkanier.addLadung(vulkanierLadung2);
	
	//Gefecht
	klingonen.torpedosAbfeuern(romulaner);
	romulaner.phaserAbfeuern(klingonen);
	vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
	klingonen.zustandAusgeben();
	klingonen.ladungsverzeichnisAusgeben();
	vulkanier.reparieren(true, true, true, 5);
	vulkanier.torpedosLaden(3);
	vulkanier.ladungsverzeichnisAufräumen();
	klingonen.torpedosAbfeuern(romulaner);
	klingonen.torpedosAbfeuern(romulaner);
	klingonen.zustandAusgeben();
	klingonen.ladungsverzeichnisAusgeben();
	romulaner.zustandAusgeben();
	romulaner.ladungsverzeichnisAusgeben();
	vulkanier.zustandAusgeben();
	vulkanier.ladungsverzeichnisAusgeben();
	System.out.println(Raumschiff.logbuchAusgeben());
	}
}