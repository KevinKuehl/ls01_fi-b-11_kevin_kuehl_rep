
public class A1_6_AB_Aufgabe_3_Tabellenaufgabe{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String f = "Fahrenheit";
		String c = "Celsius";
		double da2 = -28.8889;
		double db2 = -23.3333;
		double dc2 = -17.7778;
		double dd2 = -6.6667;
		double de2 = -1.1111;
		
		String a1 = "-20";
		String b1 = "-10";
		String c1 = "+0";
		String d1 = "+20";
		String e1 = "+30";
		String a2 = "-28.8889";
		String b2 = "-23.3333";
		String c2 = "-17.7778";
		String d2 = "-6.6667";
		String e2 = "-1.1111";
		
		System.out.print("\nA1.6 AB Aufgabe 3\n");
		
		System.out.printf("\n%-12s|", f);
		System.out.printf("%10s", c);
		System.out.printf("\n-------------------------");
		System.out.printf("\n%-12s|", a1);
		System.out.printf("%10.2f", da2);
		System.out.printf("\n%-12s|", b1);
		System.out.printf("%10.2f", db2);
		System.out.printf("\n%-12s|", c1);
		System.out.printf("%10.2f", dc2);
		System.out.printf("\n%-12s|", d1);
		System.out.printf("%10.2f", dd2);
		System.out.printf("\n%-12s|", e1);
		System.out.printf("%10.2f", de2);
	}

}
