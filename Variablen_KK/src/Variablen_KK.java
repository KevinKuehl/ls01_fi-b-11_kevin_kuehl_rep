//package eingabeAusgabe;
/** Variablen.java
    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
    @author
    @version
*/
public class Variablen_KK {
  public static void main(String [] args){
    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
          Vereinbaren Sie eine geeignete Variable */
	  int zaehler = 0;

    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	  zaehler = 25;
	  
	  System.out.printf("Der Zaehler gibt den Zahlenwert %d wieder." , zaehler);    // Syso + Strg + Leertaste
	  																				// %d ist ein Platzhalter für Zahlen. Da wird die Variable eingesetzt.
	  

    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
          eines Programms ausgewaehlt werden.
          Vereinbaren Sie eine geeignete Variable */
	  
	  char menuepunkt = 'A'; 		// char = ein Zeichen [String würde auch gehen aber da nur ein Zeichen gefordert wurde reicht char aus]
	  
	  
    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	  
	  menuepunkt = 'C';		//'C' weil es keine Zahl ist.
	  System.out.printf("\nDer Menuepunkt '%c' wurde ausgewiesen." , + menuepunkt);	//%c (char) ist ein Platzhalter für Buchstaben. // %f (flote) ist ein Platzhalter für Kommerzahlen

    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
          notwendig.
          Vereinbaren Sie eine geeignete Variable */
	  
	  int astronomische_Berechnung ;

    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie sie auf dem Bildschirm aus.*/
	  
	  astronomische_Berechnung = 299792458 ;	//299792458 ohne ' weil es Zahlen sind.
	  System.out.printf("\nDie Lichtgeschwindigkeit beträgt %d Meter pro Sekunde." , + astronomische_Berechnung) ;

    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/
	  
	  byte Vereinsmitglieder = 7 ;

    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
	  
	  System.out.println("\nDie Anzahl der Vereinsmitglieder beträgt:" + Vereinsmitglieder) ;

    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
          dem Bildschirm aus.*/
	  
	  float elektrische_Elementarladung = 0.0000000000000000001602f ;		// Am Ende der Zahl muss ein f ran damit es als Float erkannt wird bzw. konvertiert wird.
	  System.out.printf("Für die Berechnung wird die elektrische_Elementarladung %.23f benötigt." , elektrische_Elementarladung);

    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */
	  
	  boolean Zahlungseingang;
	  
    /*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variable den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/
	  
	  Zahlungseingang = true; 
	  System.out.printf("\nIst eine Zahlung erfolgt? %b" , Zahlungseingang);		//%b wegen boolean 

  }//main
}// Variablen