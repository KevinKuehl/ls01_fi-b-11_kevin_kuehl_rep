import java.util.Scanner;

public class MittelwertmitMethoden {

	public static void main(String[] args) {
		double zahlwieviele;
		int i = 0; 																		// i = Zähler
		double eingabezahl;
		double mittelwert = 0;
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Dieses Programm berechnet den Mittelwert zweier Zahlen.");
		zahlwieviele = eingabe(myScanner, "Bitte geben Sie die gewünschte Anzahl der Zahlen ein: ");
		
		do {																			//Das ist eine nachprüfende Schleife
		eingabezahl = eingabe (myScanner, "Bitte geben Sie eine Zahl ein.");
		mittelwert = mittelwert + eingabezahl; 											//Summierung aller Zahlen
		i++;	
		}
		while (zahlwieviele > i);	
	
		mittelwert = mittelwert / zahlwieviele;
		ausgabe(mittelwert);
		
		myScanner.close();
	}
	
	public static double eingabe(Scanner ms, String text ) {
		System.out.print(text);
		double zahl = ms.nextDouble();
		return zahl;
	}
	
	
	public static void ausgabe(double mittelwert) {
		System.out.println("Der errechnete Mittelwert aller Zahlen ist: " + mittelwert);
	}
}