import java.util.Scanner;
class FahrkartenautomatMitMethoden {
	
	public static void main(String[] args) {
		
		while(1 < 10) {
		
		double zuzahlenderBetrag = fahrkartenBestellungErfassen();				//Aufruf von fahrkartenBestellungErfassen
		
		double rueckgabeBetrag = fahrkartenBezahlen(zuzahlenderBetrag);			//Aufruf von fahrkartenBezahlen mit uebergabe von zuzahlenderBetrag
		
		fahrscheinausgeben();													//Aufruf von fahrscheinausgeben
		
		rueckgeldAusgeben(rueckgabeBetrag);										//Aufruf von rueckgeldAusgeben mit uebergabe von rueckgabeBetrag
		}
	}
		
	public static double fahrkartenBestellungErfassen()							// Methode zur Erfassung der Bestellung
		{																		// mit Eingabeaufforderungen fuer
		double preis = 0;	
		int auswahl;
		int anzahlTickets;
		double zuZahlenderBetrag = 0;
		Scanner ns = new Scanner(System.in);									// Ticketpreis und Anzahl der Tickets
			do {
			System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\r\n"
					+ " (1) Einzelfahrschein Regeltarif AB [3,00 EUR] \r\n"
					+ " (2) Tageskarte Regeltarif AB [8,80 EUR] \r\n"
					+ " (3) Kleingruppen-Tageskarte Regeltarif AB [25,50 EUR] \r\n"
					+ " (9) Zum Kassiervorgang \r\n"
					+ "     Welches Ticket möchten Sie kaufen? ");				// Errechnet den zu zahlenden Betrag und
		    auswahl = ns.nextInt();												// gibt diesen zurueck
		    
		    if(auswahl != 9) {
		    if(auswahl > 3 && auswahl != 9 || auswahl < 1 && auswahl != 9) {                   // != nicht gleich
		    	System.out.println("Die eingebene Zahl muss zwischen 1 und 3 liegen. Versuchen Sie es nochmal.");
		    }
		    }
				}while(auswahl > 3 || auswahl < 1 );	
				if(auswahl !=9) {		
					switch(auswahl) {
					case 1:
							preis = 3.00;
							break;
				
					case 2:
							preis = 8.80;
							break;
				
					case 3:
							preis = 25.50;
							break;
			}
			
			
			
			do {
				System.out.print("Geben Sie die Anzahl der Tickets ein: ");		
				anzahlTickets = ns.nextInt();													
			    if(anzahlTickets < 1 || anzahlTickets > 10) {
			    	System.out.println("Die eingebene Zahl muss zwischen 1 und 10 liegen. Versuchen Sie es nochmal.");
			}
				}while(anzahlTickets < 1 || anzahlTickets > 10);
			    
			zuZahlenderBetrag = preis*anzahlTickets;
			}
		return zuZahlenderBetrag;	
			
		}		
	
		public static double fahrkartenBezahlen(double zuzahlenderBetrag) 		// Methode welche mit Eingabeaufforderung den
		{																		// Eingezahlten Betrag ermittelt und den
				float eingezahlterGesamtbetrag = 0.0f;							// Rueckgabebetrag zurueckgibt
				float eingeworfeneMuenze;
				Scanner ns = new Scanner(System.in);
		       while(eingezahlterGesamtbetrag < zuzahlenderBetrag)
		       {
		    	   System.out.printf("Noch zu zahlen: %.2f Euro\n" , (zuzahlenderBetrag - eingezahlterGesamtbetrag));
		    	   System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
		    	   eingeworfeneMuenze = ns.nextFloat();
		           eingezahlterGesamtbetrag += eingeworfeneMuenze;
		       }
		      
		       return eingezahlterGesamtbetrag - zuzahlenderBetrag;
		}
		public static void fahrscheinausgeben() 								// Methode zur fahrscheinausgabe
		{																		// Wartet lediglich und gibt Text aus
			 System.out.println("\nFahrschein wird ausgegeben");				// Gibt keinen Wert zurueck
		       for (int i = 0; i < 8; i++)
		       {
		          System.out.print("=");
		          try {
					Thread.sleep(250);
				  } catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				    }
		       }
		       System.out.println("\n\n");			
		}
		public static void rueckgeldAusgeben(double rueckgabeBetrag) 			// Methode welche den Rueckgeld Betrag 
		{																		// ueber schleifen ermittelt und ausgibt
			if(rueckgabeBetrag > 0.0)											// Gibt keinen Wert zurueck
		       {
		    	   System.out.printf("Der Rueckgabebetrag in Hoehe von %.2f EURO" , rueckgabeBetrag );
		    	   System.out.println("\nwird in folgenden Muenzen ausgezahlt:");
		           while(rueckgabeBetrag >= 2.0) // 2 EURO-Muenzen
		           {
		        	  System.out.println("2 EURO");
		        	  rueckgabeBetrag -= 2.0;
		           }
		           while(rueckgabeBetrag >= 1.0) // 1 EURO-Muenzen
		           {
		        	  System.out.println("1 EURO");
		        	  rueckgabeBetrag -= 1.0;
		           }
		           while(rueckgabeBetrag >= 0.5) // 50 CENT-Muenzen
		           {
		        	  System.out.println("50 CENT");
		        	  rueckgabeBetrag -= 0.5;
		           }
		           while(rueckgabeBetrag >= 0.2) // 20 CENT-Muenzen
		           {
		        	  System.out.println("20 CENT");
		        	  rueckgabeBetrag -= 0.2;
		           }
		           while(rueckgabeBetrag >= 0.1) // 10 CENT-Muenzen
		           {
		        	  System.out.println("10 CENT");
		        	  rueckgabeBetrag -= 0.1;
		           }
		           while(rueckgabeBetrag >= 0.05)// 5 CENT-Muenzen
		           {
		        	  System.out.println("5 CENT");
		        	  rueckgabeBetrag -= 0.05;
		           }
		       }
			System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                    "vor Fahrtantritt entwerten zu lassen!\n"+
                    "Wir wuenschen Ihnen eine gute Fahrt. \n"
                    + "\n>>>>>>>>>>>>>>>>>>>>>>>\n");
			
		}
}
